1.需要安装reids   可参考doc里linux－redis安装文档单机版(参考文档).docx

2.修改leimingtech-front ,leimingtech-admin ,leimingtech-seller模块conf里
redis.properties配置文件共三个

#IP地址
第2行 redis.pool.host=101.200.205.11 //redis服务器ip
#端口号
第4行 redis.pool.port=6379 //redis端口
#密码
第6行 redis.pool.password=123456  //redis 密码

第32行 redis.unlock=true  等于false则表示关闭

3.修改leimingtech-front ,leimingtech-admin ,leimingtech-seller模块context里
applicationContext.xml 配置文件
第25行 如果这行配置被注释了请把注释解开 <value>classpath*:conf/redis.properties</value>

第52-52行  如果这行配被注释了请把注释解开 <import resource="applicationContext-cache.xml"/>