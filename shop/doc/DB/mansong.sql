/*
 Navicat Premium Data Transfer

 Source Server         : localhost56
 Source Server Type    : MySQL
 Source Server Version : 50623
 Source Host           : localhost
 Source Database       : mansong

 Target Server Type    : MySQL
 Target Server Version : 50623
 File Encoding         : utf-8

 Date: 12/02/2015 18:07:41 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `shop_p_mansong`
-- ----------------------------
DROP TABLE IF EXISTS `shop_p_mansong`;
CREATE TABLE `shop_p_mansong` (
  `mansong_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '满送活动编号',
  `mansong_name` varchar(50) NOT NULL COMMENT '活动名称',
  `quota_id` int(10) unsigned NOT NULL COMMENT '套餐编号',
  `start_time` bigint(13) unsigned NOT NULL COMMENT '活动开始时间',
  `end_time` bigint(13) unsigned NOT NULL COMMENT '活动结束时间',
  `member_id` int(10) unsigned NOT NULL COMMENT '用户编号',
  `store_id` int(10) unsigned NOT NULL COMMENT '店铺编号',
  `member_name` varchar(50) NOT NULL COMMENT '用户名',
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `state` tinyint(1) unsigned NOT NULL COMMENT '状态(1-新申请/2-审核通过/3-已取消/4-审核失败)',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`mansong_id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='满就送活动表';

-- ----------------------------
--  Records of `shop_p_mansong`
-- ----------------------------
BEGIN;
INSERT INTO `shop_p_mansong` VALUES ('57', '满100减10，满500减50，满1000减100', '1', '1448856540000', '1451534940000', '5', '9', 'gyh99', '京川牦牛', '2', '11111');
COMMIT;

-- ----------------------------
--  Table structure for `shop_p_mansong_quota`
-- ----------------------------
DROP TABLE IF EXISTS `shop_p_mansong_quota`;
CREATE TABLE `shop_p_mansong_quota` (
  `quota_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '满就送套餐编号',
  `apply_id` int(10) unsigned NOT NULL COMMENT '申请编号',
  `member_id` int(10) unsigned NOT NULL COMMENT '用户编号',
  `store_id` int(10) unsigned NOT NULL COMMENT '店铺编号',
  `member_name` varchar(50) NOT NULL COMMENT '用户名',
  `store_name` varchar(50) NOT NULL COMMENT '店铺名称',
  `start_time` bigint(13) unsigned NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) unsigned NOT NULL COMMENT '结束时间',
  `state` tinyint(1) unsigned DEFAULT NULL COMMENT '配额状态(1-可用/2-取消/3-结束)',
  PRIMARY KEY (`quota_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='满就送套餐表';

-- ----------------------------
--  Records of `shop_p_mansong_quota`
-- ----------------------------
BEGIN;
INSERT INTO `shop_p_mansong_quota` VALUES ('1', '1', '1', '9', '1', '京川牦牛', '1447665747106', '1448665747106', '1');
COMMIT;

-- ----------------------------
--  Table structure for `shop_p_mansong_rule`
-- ----------------------------
DROP TABLE IF EXISTS `shop_p_mansong_rule`;
CREATE TABLE `shop_p_mansong_rule` (
  `rule_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则编号',
  `mansong_id` int(10) unsigned NOT NULL COMMENT '活动编号',
  `level` tinyint(1) unsigned DEFAULT NULL COMMENT '规则级别(1/2/3)',
  `price` decimal(10,2) unsigned NOT NULL COMMENT '级别价格',
  `shipping_free` tinyint(1) unsigned DEFAULT NULL COMMENT '免邮标志(0-不免邮/1-免邮费)',
  `discount` decimal(10,2) unsigned NOT NULL COMMENT '减现金优惠金额',
  `gift_name` varchar(50) DEFAULT NULL COMMENT '礼品名称',
  `gift_link` varchar(100) DEFAULT NULL COMMENT '礼品链接',
  PRIMARY KEY (`rule_id`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='满就送活动规则表';

-- ----------------------------
--  Records of `shop_p_mansong_rule`
-- ----------------------------
BEGIN;
INSERT INTO `shop_p_mansong_rule` VALUES ('56', '57', '2', '500.00', null, '50.00', null, null), ('55', '57', '1', '100.00', null, '10.00', null, null), ('57', '57', '3', '1000.00', null, '100.00', null, null);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
