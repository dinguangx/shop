﻿/*示例
mysql升级时，需设置sql编码为utf8
set names utf8;

修改字段的用途 
update tableName set ListPageSize=20;

彻底删除表
drop table if exists tableName;

删除表数据
delete from tableName;

删除表数据 
TRUNCATE table name

建表
create table tableName
(
   ID                   bigint not null,
   Name                 varchar(100) not null,
   IsOpen               varchar(2) not null,
   Description          varchar(500),
   MessageCount         varchar(20),
   Prop1                varchar(50),
   Prop2                varchar(50),
   Prop3                varchar(255),
   AddUser              varchar(200) not null,
   AddTime              datetime not null,
   ModifyUser           varchar(200),
   ModifyTime           datetime,
   primary key (ID)
);

插入数据
insert into tableName (id,name, prop, prop1) values(1,'test1','1',null);

更新数据
update tableName set URL='Workflow/WorkflowList.jsp' where URL='Platform/Workflow.jsp';

在tableName里 ImageSource字段的后面新增test字段长度为100 
ALTER TABLE tableName ADD COLUMN test varchar(100) AFTER ImageSource;

删除表 tableName里的prop1字段
ALTER TABLE tableName DROP COLUMN prop1;

修改字段的长度
ALTER TABLE tableName MODIFY COLUMN Memo VARCHAR(400);

*/

/**新导入的数据库需要修改 shop_store   store_id=0**/
update shop_store set store_id=0 where member_id=0

/***赵荣华 201509171626 ***/
/*在shop_evaluate_goods里 update_time字段的后面新增spec_info字段 长度为255  */
ALTER TABLE shop_evaluate_goods ADD COLUMN spec_info varchar(255) AFTER update_time;


/*** 信誉评价表  shop_evaluate_goods ***/
/* 赵荣华2015-09-17 shop_evaluate_goods表评价时间字段 */
ALTER TABLE shop_evaluate_goods MODIFY COLUMN  geval_addtime bigint(13)  comment '评价时间';

/**
 * 修改会员表和管理员表密码长度
 * KVIUFF 20150923 00:08
 */
alter table shop_admin modify column admin_password varchar(300) ;
alter table shop_member modify column member_passwd varchar(300) ;

/**
 * 修改字段长度
 * linjm 2015-09-23
 */
ALTER TABLE shop_store_words MODIFY COLUMN keyword VARCHAR(100);
ALTER TABLE shop_goods_words MODIFY COLUMN keyword VARCHAR(100);

/* liukai 2015-09-23 shop_member会员表会员等级积分字段 */
ALTER TABLE shop_member CHANGE member_points member_rank_points int(11) comment '会员等级积分' AFTER member_info;

/* liukai 2015-09-23 shop_member会员表会员消费积分字段 */
ALTER TABLE shop_member ADD COLUMN member_consume_points int(11) comment '会员消费积分' AFTER member_info;

/* liukai 2015-09-23 shop_setting系统设置表 */
INSERT INTO `shop_setting` VALUES ('points', '{\"buygoods_cons\":\"1\",\"goodsfirstcom_cons\":\"50\",\"email_cons\":\"20\",\"comment_rank\":\"10\",\"recharge_cons\":\"50\",\"login_cons\":\"5\",\"sign_cons\":\"5\",\"register_rank\":\"50\",\"uppiccom_rank\":\"30\",\"uppiccom_cons\":\"30\",\"sign_rank\":\"5\",\"recharge_rank\":\"0\",\"register_cons\":\"50\",\"buygoods_rank\":\"1\",\"onlinepay_cons\":\"10\",\"goodsfirstcom_rank\":\"50\",\"comment_cons\":\"10\",\"login_rank\":\"5\",\"email_rank\":\"20\",\"onlinepay_rank\":\"10\",\"recfriend_rank\":\"0\",\"recfriend_cons\":\"50\",\"persdata_cons\":\"20\",\"persdata_rank\":\"20\"}');

/* 郭艳辉2015-09-23 shop_store店铺表创建时间 */

ALTER TABLE shop_store CHANGE create_time storecreate_time bigint(13);

/* 2015-09-24 给广告修改时间 */
update shop_adv set start_date=1443066281396,end_date=1453066281396
/* 2015-09-24 给商品修改时间 */
update shop_goods set update_time=1443066281396,start_time=1443066281396,end_time=1453066281396

/* 2015-09-24 修改文章时间 */
update shop_article  create_time=1443066281396

/*2015年9月24日 16:17:24 liuzhen 京东联盟*/
drop table if exists union_goods_class;

/*==============================================================*/
/* Table: union_goods_class   联盟商品分类表                                   */
/*==============================================================*/
create table union_goods_class
(
   id                   int(11) not null auto_increment comment '主键',
   name                 varchar(100) comment '分类名称',
   icon                 varchar(100) comment '图标',
   show_flag            int(1) comment '是否显示（0：不显示；1：显示）',
   sort                 int(10) comment '排序',
   create_by            int(11) comment '创建者',
   create_date          bigint(13) comment '创建日期',
   remarks              varchar(500) comment '备注',
   del_flag             int(1) comment '删除标记（0：正常；1：删除；2：审核）',
   update_by            int(11) comment '更新者',
   update_date          bigint(13) comment '更新日期',
   primary key (id)
);

alter table union_goods_class comment '联盟商品分类';


drop table if exists union_goods;

/*==============================================================*/
/* Table: union_goods    联盟商品表                                     */
/*==============================================================*/
create table union_goods
(
   id                   int(11) not null auto_increment comment '主键',
   class_id             int(11) comment '分类',
   goodsName            varchar(100) comment '商品名称',
   image_url            varchar(500) comment '商品图片',
   skuId                int(11) comment 'skuId',
   unitPrice            double(10,1) comment '商品单价即京东价（单价为-1表示未查询到改商品单价）',
   commisionRatioPc     double(10,2) comment 'PC佣金比例',
   commisionRatioWl     double(10,2) comment '无线佣金比例',
   shopId               int(11) comment '店铺ID',
   materialUrl          varchar(500) comment '商品落地页',
   startDate            bigint(13) comment '推广开始日期',
   endDate              bigint(13) comment '推广结束日期',
   source               int(1) comment '商品来源（例如：京东、淘宝）目前值只有"1"代表京东',
   show_flag            int(1) comment '是否显示（0：不显示；1：显示）',
   sort                 int(10) comment '排序',
   create_by            varchar(32) comment '创建者',
   create_date          bigint(13) comment '创建日期',
   remarks              varchar(500) comment '备注',
   del_flag             char(1) comment '删除标记（0：正常；1：删除；2：审核）',
   update_by            varchar(32) comment '更新者',
   update_date          bigint(13) comment '更新日期',
   primary key (id)
);

alter table union_goods comment '联盟商品';

/* 郭艳辉2015-09-25 shop_role店铺表 修改创建时间类型 */
update shop_role set create_time=null;
ALTER TABLE shop_role MODIFY COLUMN  create_time bigint(13)  comment '创建时间';

/* liukai2015-09-28 shop_consult查询咨询表 修改咨询发布时间类型 */
ALTER TABLE shop_consult MODIFY COLUMN  consult_addtime bigint(13) comment '咨询发布时间';

/* liukai2015-09-29 shop_consult查询咨询表 修改咨询回复时间类型 */
ALTER TABLE shop_consult MODIFY COLUMN consult_reply_time BIGINT(13) COMMENT '咨询回复时间';

/* 郭艳辉2015-09-29 shop_favorites买家收藏表 修改收藏时间类型 */
update shop_favorites set fav_time=null;
ALTER TABLE shop_favorites MODIFY COLUMN  fav_time bigint(13)  comment '收藏时间';

/**
 * kviuff 2015-10-10 17:28:00
 * shop_consult商品咨询表修改回复时间和添加时间
 */
ALTER TABLE shop_consult MODIFY COLUMN  consult_reply_time bigint(13)  comment '咨询回复时间';
ALTER TABLE shop_consult MODIFY COLUMN  consult_addtime bigint(13)  comment '咨询添加时间';


/*2015年10月9日 15:21:05 liuzhen 在联盟商品分类中添加新字段*/
ALTER TABLE `union_goods_class`
add column `pid`  int(11) null default 0 comment '父id',
add column `levels`  int(1) null default 1 comment '层级',
add column `idpaths`  varchar(2000) null default null comment 'idpaths';

/*2015年10月9日 15:21:05 liuzhen 给idpaths默认值*/
update union_goods_class ugc set idpaths=concat(ugc.id,',');

/*2015年10月10日 11:42:34 liuzhen 增加class_ids字段*/
ALTER TABLE `union_goods`
add column `class_ids`  varchar(500) null default null comment '分类ids';
/*2015年10月10日 11:45:33 liuzhen 给class_ids字段赋值*/
update union_goods ug set ug.class_ids=(select ugc.idpaths from union_goods_class ugc where ugc.id=ug.class_id);

/*2015年10月12日 09:59:34 liuzhen 在联盟商品分类中添加新字段*/
ALTER TABLE `union_goods_class`
add column `names`  varchar(500) null default null comment '所有分类名称';

/*2015年10月11日 14:51:39 liuzhen 商品表新增分类名称字段*/
ALTER TABLE `union_goods`
add column `class_name`  varchar(100) null default null comment '分类名称',
add column `class_names`  varchar(500) null default null comment '所有分类名称';
/*2015年10月11日 14:51:39 liuzhen 给class_name字段赋值*/
update union_goods ug set 
ug.class_name=(select ugc.name from union_goods_class ugc where ugc.id=ug.class_id),
ug.class_names=(select ugc.name from union_goods_class ugc where ugc.id=ug.class_id);

/* liukai2015-10-12 shop_order_address订单收货地址信息表  */
DROP TABLE IF EXISTS `shop_order_address`;
CREATE TABLE `shop_order_address` (
  `address_id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '地址ID',
  `member_id` mediumint(10) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `true_name` varchar(50) NOT NULL COMMENT '会员姓名',
  `area_id` mediumint(10) unsigned NOT NULL DEFAULT '0' COMMENT '地区ID',
  `city_id` mediumint(9) DEFAULT NULL COMMENT '市级ID',
  `area_info` varchar(255) NOT NULL DEFAULT '' COMMENT '地区内容',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `tel_phone` varchar(20) DEFAULT NULL COMMENT '座机电话',
  `mob_phone` varchar(15) DEFAULT NULL COMMENT '手机电话',
  `is_default` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1默认收货地址',
  `province_id` mediumint(10) DEFAULT '0' COMMENT '省级id',
  `zip_code` int(50) DEFAULT NULL COMMENT '邮编',
  PRIMARY KEY (`address_id`),
  KEY `member_id` (`member_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='订单地址信息表';

/* liukai2015-10-12 shop_order_daddress订单发货地址信息表  */
DROP TABLE IF EXISTS `shop_order_daddress`;
CREATE TABLE `shop_order_daddress` (
  `address_id` mediumint(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '地址ID',
  `store_id` mediumint(10) unsigned NOT NULL DEFAULT '0' COMMENT '店铺ID',
  `seller_name` varchar(50) NOT NULL DEFAULT '' COMMENT '联系人',
  `area_id` mediumint(10) unsigned NOT NULL COMMENT '地区ID',
  `city_id` mediumint(9) DEFAULT NULL COMMENT '市级ID',
  `area_info` varchar(255) NOT NULL COMMENT '地区内容',
  `address` varchar(255) NOT NULL COMMENT '地址',
  `zip_code` int(50) DEFAULT NULL COMMENT '邮编',
  `tel_phone` varchar(20) DEFAULT NULL COMMENT '座机电话',
  `mob_phone` varchar(15) DEFAULT NULL COMMENT '手机电话',
  `company` varchar(255) NOT NULL COMMENT '公司',
  `content` text COMMENT '备注',
  `is_default` enum('0','1') NOT NULL DEFAULT '0' COMMENT '是否默认1是',
  `province_id` mediumint(10) DEFAULT NULL COMMENT '省级id',
  PRIMARY KEY (`address_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='订单发货地址信息表';

/* liukai 2015-10-15 shop_order订单表 增加字段cancel_cause订单取消原因*/
ALTER TABLE shop_order ADD COLUMN cancel_cause varchar(50) comment '订单取消原因';

/* liukai 2015-10-15 shop_order订单表 增加字段order_total_price订单总价格 */
ALTER TABLE shop_order ADD COLUMN order_total_price decimal(10,2) COMMENT '订单总价格' AFTER order_amount;

/* liukai 2015-10-15 shop_order订单表 修改字段order_amount订单应付金额*/
ALTER TABLE shop_order MODIFY COLUMN order_amount decimal(10,2) COMMENT '订单应付金额';

/* kviuff 2015-10-16 shop_type商品类型表 修改字段type_sort商品类型排序*/
ALTER TABLE shop_type MODIFY COLUMN type_sort int(3) COMMENT '商品类型排序';
alter table shop_type alter column type_sort set default 0;

/*2015年10月22日  wanghao 在菜单表中添加权限标识字段*/
ALTER TABLE `shop_menu`
add column `m_permission`  varchar(500) null default null comment '权限标识';

/* liukai 2015-10-22 shop_order_daddress订单发货地址信息表 增加字段order_id订单id */
ALTER TABLE shop_order_daddress ADD COLUMN order_id varchar(255) COMMENT '订单id';

/*2015年10月26日  wanghao 在菜单表中添加是否显示字段*/
ALTER TABLE shop_menu 
add COLUMN m_isshow int(3) COMMENT '是否显示：0,否;1,是';
alter table shop_menu alter column m_isshow set default 0;

/*2015年10月26日  wanghao 修改菜单表中是否显示字段为显示*/
update shop_menu set m_isshow=1


/*zhaorh 2015-10-26 shop_setting系统设置表 */
INSERT INTO `shop_setting` VALUES ('images', '{\"big_pic_height\":\"6\",\"thumbnail_pic_height\":\"5\",\"tiny_pic_width\":\"33\",\"thumbnail_pic_width\":\"22\",\"update_val\":\"\",\"big_pic_width\":\"55\",\"small_pic_height\":\"66\",\"tiny_pic_height\":\"33\",\"small_pic_width\":\"35\"}'), 
('site', '{\"logo\":\"/upload/img/brand/1445856523981.jpg\",\"siteKey\":\"leimingkey\",\"siteName\":\"leimingtech\",\"siteDiscription\":\"ccc\",\"siteTitle\":\"leiming\"}');

/*2015年10月27日  gyh  在shop_store_grade店铺等级表中添加佣金比例字段*/
ALTER TABLE shop_store_grade 
add COLUMN brokerage_scale float(0) COMMENT '佣金比例' AFTER is_del;

/*2015年10月27日  liukai  在shop_order订单表 修改字段discount优惠总金额*/
ALTER TABLE shop_order MODIFY COLUMN discount decimal(10,2) COMMENT '优惠总金额';

/*2015年10月27日  liukai  在shop_order订单表 添加字段coupon_id优惠券id*/
ALTER TABLE shop_order ADD COLUMN coupon_id int(11) COMMENT '优惠券id';

/*2015年10月27日  liukai  在shop_order订单表 添加字段coupon_price优惠券金额*/
ALTER TABLE shop_order ADD COLUMN coupon_price decimal(10,2) COMMENT '优惠券金额';

/*2015年10月27日  liukai  在shop_order订单表 添加字段promo_price促销金额*/
ALTER TABLE shop_order ADD COLUMN promo_price decimal(10,2) COMMENT '促销金额';

/*2015年10月27日  liukai  在shop_order_goods订单商品表 添加字段goods_pay_price商品实际成交价*/
ALTER TABLE shop_order_goods ADD COLUMN goods_pay_price decimal(10,2) COMMENT '商品实际成交价';

/*2015年10月28日  gyh  在shop_store_goods_class店铺自定义商品分类表中添加是否审核通过*/
ALTER TABLE shop_store_goods_class 
add COLUMN check_state int(9) COMMENT '是否审核通过，0审核中，1审核通过，2审核不通过' AFTER is_del;
update shop_store_goods_class set check_state=1;

/*2015年10月29日  gyh  在shop_store_goods_class店铺自定义商品分类表中添加审核结果*/
ALTER TABLE shop_store_goods_class 
add COLUMN  reason varchar(290) COMMENT '审核结果' AFTER check_state;

/********************费用比例**********************/
/*2015年10月29日  gyh  在shop_type商品类型表中添加父id,层级path*/
ALTER TABLE shop_type 
add COLUMN  st_parent_id int(10) COMMENT '父id' AFTER type_sort;

ALTER TABLE shop_type 
add COLUMN  st_idpath varchar(199) COMMENT '层级path' AFTER st_parent_id;

/*2015年10月29日  gyh  在shop_type中把添加费用比例*/
ALTER TABLE shop_type 
add COLUMN  expen_scale float(0) COMMENT '费用比例' AFTER st_idpath;

/*2015年10月29日  gyh  在shop_type中把所有的父id置为0*/
update shop_type set st_parent_id=0;

/*2015年10月29日  gyh  在shop_type中把所有的st_idpath置为type_id加逗号*/
update shop_type set st_idpath = concat(type_id,',');

/********************费用比例**********************/

/*2015年10月30日  zhaorh  在shop_consult修改consult_reply_time字段的类型*/
alter table shop_consult
MODIFY column consult_reply_time bigint(13)

/*2015年10月30日  liukai  在shop_order订单表 添加字段lock_state锁定状态*/
ALTER TABLE shop_order ADD COLUMN lock_state tinyint(1) DEFAULT '0' COMMENT '锁定状态:0是正常,大于0是锁定,默认是0';

/*2015年10月30日  liukai  在shop_order_goods订单商品表 添加字段buyer_id买家ID*/
ALTER TABLE shop_order_goods ADD COLUMN buyer_id int(11)  COMMENT '买家ID';

/*2015年11月4日  liukai  删除shop_return退货表*/
DROP TABLE IF EXISTS `shop_return`;

/*2015年11月4日  liukai  删除shop_return_goods退货商品表*/
DROP TABLE IF EXISTS `shop_return_goods`;

/*2015年11月3日  zhaorh  在shop_setting系统设置中添加缓存设置*/
INSERT INTO shop_setting VALUES ('images', '{\"big_pic_height\":\"400\",\"thumbnail_pic_height\":\"200\",\"tiny_pic_width\":\"100\",\"thumbnail_pic_width\":\"200\",\"update_val\":\"\",\"big_pic_width\":\"400\",\"small_pic_height\":\"300\",\"tiny_pic_height\":\"100\",\"small_pic_width\":\"300\"}'), ('points', '{\"email_cons\":\"20\",\"uppiccom_rank\":\"30\",\"register_rank\":\"50\",\"persdata_cons\":\"20\",\"comment_cons\":\"10\",\"buygoods_rank\":\"1\",\"goodsfirstcom_cons\":\"50\",\"sign_rank\":\"5\",\"goodsfirstcom_rank\":\"50\",\"persdata_rank\":\"20\",\"buygoods_cons\":\"1\",\"comment_rank\":\"10\",\"login_cons\":\"5\",\"recfriend_rank\":\"5\",\"recharge_rank\":\"0\",\"recfriend_cons\":\"50\",\"uppiccom_cons\":\"30\",\"email_rank\":\"20\",\"onlinepay_cons\":\"10\",\"recharge_cons\":\"50\",\"login_rank\":\"10\",\"sign_cons\":\"5\",\"register_cons\":\"50\",\"onlinepay_rank\":\"10\"}'), ('redisCache', '{\"dic_seconds\":\"664\",\"adv_seconds\":\"555\",\"category_seconds\":\"6666\",\"dic_isShow\":\"0\",\"adv_isShow\":\"1\",\"other_isShow\":\"1\",\"other_seconds\":\"43\",\"category_isShow\":\"0\"}'), ('site', '{\"logo\":\"/1446023038117.jpg\",\"siteKey\":\"leimingkey2222\",\"siteName\":\"leimingtech2222\",\"siteDiscription\":\"leimingkeyleimingkey22222\",\"siteTitle\":\"leiming22222\"}');

/*2015年11月4日  gyh  在shop_store_class店铺分类表中添加保证金*/
ALTER TABLE shop_store_class 
add COLUMN  margin int(19) COMMENT '保证金' AFTER is_del;


/*2015年11月3日  zhaorh  在shop_menu添加敏感库上传和系统设置菜单*/
INSERT INTO shop_menu (m_name,m_url,m_parent_id,m_sort,m_level,m_path,m_description,m_permission,m_isshow)
VALUES ( '系统设置', '/setting/points/index', '16', '0', '2', '0,16,', '积分设置', '', '1');

INSERT INTO shop_menu (m_name,m_url,m_parent_id,m_sort,m_level,m_path,m_description,m_permission,m_isshow)
VALUES ( '敏感词库上传', '/setting/sensitive/add', '16', '0', '2', '0,16,', '', null, '1');
 				

/*2015年11月4日  gyh  在shop_goods_class中添加费用比例*/
ALTER TABLE shop_goods_class 
add COLUMN  expen_scale float(0) COMMENT '费用比例' AFTER gc_idpath;
/*2015年11月4日  gyh  在shop_goods_class中添加是否关联子分类*/
ALTER TABLE shop_goods_class 
add COLUMN  is_relate tinyint(1) COMMENT '是否关联子分类 0否, 1是' AFTER expen_scale; 

/* liukai 2015-11-5 shop_order订单表 修改字段order_sn订单编号*/
ALTER TABLE shop_order MODIFY COLUMN order_sn varchar(50) COMMENT '订单编号';

/* liukai 2015-11-5 shop_order订单表 修改字段pay_sn订单编号*/
ALTER TABLE shop_order MODIFY COLUMN pay_sn varchar(50) COMMENT '订单支付表编号';

/* liukai 2015-11-5 shop_order_pay订单支付表 修改字段pay_sn支付单号*/
ALTER TABLE shop_order_pay MODIFY COLUMN pay_sn varchar(50) COMMENT '支付单号';	

/* zhaorh 2015-11-5 shop_setting系统设置表 插入咨询设置的数据*/
INSERT INTO shop_setting VALUES ('consult', '{\"consult_isShow\":\"0\"}');

/* zhaorh 2015-11-5 shop_setting系统设置表 插入商品审核设置的数据*/
INSERT INTO shop_setting VALUES  ('goods_isApply', '{\"goods_isApply\":\"0\"}');

/* zhaorh 2015-11-6 shop_goods 改变违规下架商品状态值*/
update shop_goods b 
set  b.goods_state = 40
where b.goods_state = 1;

/* zhaorh 2015-11-6 shop_goods 改变审核通过商品状态值*/
update shop_goods b 
set  b.goods_state = 30
where b.goods_state = 0;

/* liukai 2015-11-9 shop_store_sns_tracelog店铺动态表 修改更新时间字段update_time*/
ALTER TABLE shop_store_sns_tracelog MODIFY COLUMN update_time BIGINT(13);

/* liukai 2015-11-9 shop_goods表 修改商品审核状态*/
UPDATE shop_goods s SET  s.goods_state= 30
WHERE s.goods_state= 0;

UPDATE shop_goods s SET  s.goods_state= 1
WHERE s.goods_state= 40;

/*2015年11月11日  gyh  在shop_rel_goods_recommend中添加排序字段*/
ALTER TABLE shop_rel_goods_recommend 
add COLUMN  sort int(4) COMMENT '排序' AFTER goods_id;

/**
 * kviuff 2015-11-12 修改商品表规格名称字段类型
 */
alter table shop_goods modify column spec_name longtext comment '规格名称';
/**
 * zhaorh 2015-11-13 修改商品表的商品发布时间
 */
UPDATE shop_goods set create_time=1444630085251
/**
 * zhaorh 2015-11-16 修改setting表站点设置增加退货时间字段
 */
delete from shop_setting;
INSERT INTO `shop_setting` VALUES ('consult', '{\"consult_isShow\":\"0\"}'), ('goods_isApply', '{\"goods_isApply\":\"0\"}'), ('images', '{\"big_pic_height\":\"400\",\"thumbnail_pic_height\":\"200\",\"tiny_pic_width\":\"100\",\"thumbnail_pic_width\":\"200\",\"update_val\":\"\",\"big_pic_width\":\"400\",\"small_pic_height\":\"300\",\"tiny_pic_height\":\"100\",\"small_pic_width\":\"300\"}'), ('points', '{\"email_cons\":\"20\",\"uppiccom_rank\":\"30\",\"register_rank\":\"50\",\"persdata_cons\":\"20\",\"comment_cons\":\"10\",\"buygoods_rank\":\"1\",\"goodsfirstcom_cons\":\"50\",\"sign_rank\":\"5\",\"goodsfirstcom_rank\":\"50\",\"persdata_rank\":\"20\",\"buygoods_cons\":\"1\",\"comment_rank\":\"10\",\"login_cons\":\"5\",\"recfriend_rank\":\"5\",\"recharge_rank\":\"0\",\"recfriend_cons\":\"50\",\"uppiccom_cons\":\"30\",\"email_rank\":\"20\",\"onlinepay_cons\":\"10\",\"recharge_cons\":\"50\",\"login_rank\":\"10\",\"sign_cons\":\"5\",\"register_cons\":\"50\",\"onlinepay_rank\":\"10\"}'), ('redisCache', '{\"dic_seconds\":\"664\",\"adv_seconds\":\"555\",\"category_seconds\":\"6666\",\"dic_isShow\":\"1\",\"adv_isShow\":\"1\",\"other_isShow\":\"1\",\"other_seconds\":\"43\",\"category_isShow\":\"1\"}'), ('site', '{\"logo\":\"/1447640546425.jpg\",\"returnTime\":\"7\",\"siteKey\":\"leimingkey2222\",\"siteName\":\"leimingtech2222\",\"siteDiscription\":\"leimingkeyleimingkey22222\",\"siteTitle\":\"leiming22222\"}');

/**
 * kviuff 2015-11-18 添加商品规格表是否开启规格字段类型
 */
alter table shop_goods_spec add COLUMN spec_isopen int(1) DEFAULT 1 comment '是否开启规格,1:开启，0:关闭';

/**
 * liukai 2015-11-19 修改shop_points_log会员积分日志表增加积分操作类型字段pl_type
 */
 ALTER TABLE shop_points_log ADD COLUMN pl_type int(3) COMMENT '积分操作类型';
 
 /**
 * liukai 2015-11-19 修改shop_order订单表增加支付分支字段payment_branch
 */
 ALTER TABLE shop_order ADD COLUMN payment_branch varchar(20) COMMENT '支付分支' AFTER payment_code;
 
 /**
 * liukai 2015-11-19 修改shop_order订单表增加交易流水号字段trade_sn
 */
 ALTER TABLE shop_order ADD COLUMN trade_sn varchar(50) COMMENT '交易流水号' AFTER out_sn;
 

/*zhaorh 2015-11-23 修改shop_return_log表state_info字段长度为255  */
ALTER TABLE shop_return_log MODIFY COLUMN state_info VARCHAR(255) NOT NULL COMMENT '退货状态描述';

/*zhaorh 2015-11-23 shop_menu增加退货审核  */
INSERT INTO `shop_menu` (`m_id`, `m_name`, `m_url`, `m_parent_id`, `m_sort`, `m_level`, `m_path`, `m_description`, `m_permission`, `m_isshow`) VALUES ('232', '退款审核', '/orders/returnList', '20', '0', '2', '0,20,', '', '', '1');
INSERT INTO `shop_menu` (`m_id`, `m_name`, `m_url`, `m_parent_id`, `m_sort`, `m_level`, `m_path`, `m_description`, `m_permission`, `m_isshow`) VALUES ('233', '查看', '', '232', '0', '3', '0,20,232,', '', 'sys:return:view', '1');
INSERT INTO `shop_menu` (`m_id`, `m_name`, `m_url`, `m_parent_id`, `m_sort`, `m_level`, `m_path`, `m_description`, `m_permission`, `m_isshow`) VALUES ('234', ' 修改', '', '232', '0', '3', '0,20,232,', '', 'sys:return:edit', '1');

/**
 * liukai 2015-11-24 修改shop_refund_return退款退货表增加退款批次号字段batch_no
 */
ALTER TABLE shop_refund_return ADD COLUMN batch_no VARCHAR(50) COMMENT '退款批次号';

/**
 * liukai 2015-11-27 修改shop_order_goods订单商品表增加佣金比例字段commis_rate
 */
ALTER TABLE shop_order_goods ADD COLUMN commis_rate FLOAT COMMENT '佣金比例';

/**
 * liukai 2015-11-27 修改shop_order_goods订单商品表增加商品最底级分类ID字段gc_id
 */
ALTER TABLE shop_order_goods ADD COLUMN gc_id int(10) COMMENT '商品最底级分类ID';

/**
 * lkang 2015-11-30 商品表添加市场价、成本价字段
 */
ALTER TABLE shop_goods ADD COLUMN goods_market_price decimal(10,2) COMMENT '市场价';
ALTER TABLE shop_goods ADD COLUMN goods_cost_price decimal(10,2) COMMENT '成本价';

/**
 * liukai 2015-11-30 修改shop_order_bill订单结算表增加结算单年份字段os_year
 */
ALTER TABLE shop_order_bill ADD COLUMN os_year int(6) COMMENT '结算单年份' AFTER os_month;

/**
 * liukai 2015-12-5 修改shop_cart购物车表增加商品所在一级分类的id字段first_gc_id
 */
ALTER TABLE shop_cart ADD COLUMN first_gc_id int(10) COMMENT '商品所在一级分类的id';

/**
 * yangxiaoping 2015-12-8 修改shop_coupon优惠券表添加商品分类id字段coupon_goods_class_id
 */
ALTER TABLE shop_coupon ADD COLUMN coupon_goods_class_id int(10) COMMENT '商品分类id';

**
 * gyh 2015-12.11 修改shop_menu菜单表增加成交商品统计
 */
INSERT INTO shop_menu (m_name,m_url,m_parent_id,m_sort,m_level,m_path,m_description,m_permission,m_isshow) VALUES ('成交商品统计', '/platform/report/storeGoodsSales', '88', '0', '2', '0,88,', '成交商品统计', '', '1');
/**
 * gyh 2015-12.11 修改shop_menu菜单表修改订单统计url
 */
UPDATE shop_menu set m_id='101',m_name='订单统计',m_url='/platform/report/orderIndex',m_parent_id='88',m_sort='0',m_level='2',m_path='0,88,',m_description='订单统计',m_permission='',m_isshow='1' WHERE m_id='101';
/**
 * gyh 2015-12.11 修改shop_menu菜单表修改流量统计url
 */
UPDATE shop_menu SET m_id='100',m_name='流量统计',m_url='/platform/report/clickIndex',m_parent_id='88',m_sort='0',m_level='2',m_path='0,88,',m_description='流量统计',m_permission='', `m_isshow`='1' WHERE m_id='100';
/**
 * gyh 2015-12.11 修改shop_menu菜单表修改店铺商品自定义分类url
 */
INSERT INTO shop_menu (m_name,m_url,m_parent_id,m_sort,m_level,m_path,m_description,m_permission,m_isshow) VALUES ('店铺商品分类', '/storeGoodsClass/list', '18', '0', '2', '0,18,', '店铺商品分类', '', '1');

/**
 * liukai 2015-12-12 修改shop_menu菜单表增加结算相关路径
 */
INSERT INTO shop_menu(m_id, m_name, m_url, m_parent_id, m_sort, m_level, m_path, m_description, m_permission, m_isshow) VALUES ('235', '结算管理', '/bill/list', '87', '0', '2', '0,87,', '', '', '1');
INSERT INTO shop_menu(m_id, m_name, m_url, m_parent_id, m_sort, m_level, m_path, m_description, m_permission, m_isshow) VALUES ('236', '查看', '', '235', '0', '3', '0,87,235,', '', 'sys:orderbill:view', '1');
INSERT INTO shop_menu(m_id, m_name, m_url, m_parent_id, m_sort, m_level, m_path, m_description, m_permission, m_isshow) VALUES ('237', '修改', '', '235', '0', '3', '0,87,235,', '', 'sys:orderbill:edit', '1');
INSERT INTO shop_menu(m_id, m_name, m_url, m_parent_id, m_sort, m_level, m_path, m_description, m_permission, m_isshow) VALUES ('238', '结算设置', '/setting/bill/index', '16', '0', '2', '0,16,', '', '', '1');
INSERT INTO shop_menu(m_id, m_name, m_url, m_parent_id, m_sort, m_level, m_path, m_description, m_permission, m_isshow) VALUES ('239', '查看', '', '238', '0', '3', '0,16,238,', '', 'sys:billSetting:view', '1');
INSERT INTO shop_menu(m_id, m_name, m_url, m_parent_id, m_sort, m_level, m_path, m_description, m_permission, m_isshow) VALUES ('240', '修改', '', '238', '0', '3', '0,16,238,', '', 'sys:billSetting:edit', '1');

/**
 * liukai 2015-12-12 删除shop_menu菜单表删除多余数据
 */
delete from shop_menu where m_name = '订单结算'



/**
 * zhaorh 2015-12.12 修改shop_navigation增加导航菜单
 */
INSERT INTO `shop_navigation` ( `nav_type`, `nav_title`, `nav_url`, `nav_location`, `nav_new_open`, `nav_sort`, `item_id`, `is_del`, `create_time`, `update_time`) 
VALUES ( '0', '首页', '', '0', '1', '255', '0', '0', '1449903649670', '1449905113103');

INSERT INTO `shop_navigation` ( `nav_type`, `nav_title`, `nav_url`, `nav_location`, `nav_new_open`, `nav_sort`, `item_id`, `is_del`, `create_time`, `update_time`) 
VALUES ( '0', '品牌', '/leimingtech-front/all/brand', '0', '1', '255', '0', '0', '1449903649670', '1449905113103');

INSERT INTO `shop_navigation` ( `nav_type`, `nav_title`, `nav_url`, `nav_location`, `nav_new_open`, `nav_sort`, `item_id`, `is_del`, `create_time`, `update_time`) 
VALUES ( '0', '优惠券', '/leimingtech-front/all/coupon', '0', '1', '255', '0', '0', '1449903649670', '1449905113103');

INSERT INTO `shop_navigation` ( `nav_type`, `nav_title`, `nav_url`, `nav_location`, `nav_new_open`, `nav_sort`, `item_id`, `is_del`, `create_time`, `update_time`) 
VALUES ( '0', '积分中心', '/leimingtech-front/points/index', '0', '1', '255', '0', '0', '1449903649670', '1449905113103');
