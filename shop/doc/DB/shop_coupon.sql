/*
Navicat MySQL Data Transfer

Source Server         : yxp
Source Server Version : 50540
Source Host           : localhost:3306
Source Database       : b2b2c

Target Server Type    : MYSQL
Target Server Version : 50540
File Encoding         : 65001

Date: 2015-12-02 19:45:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for shop_coupon
-- ----------------------------
DROP TABLE IF EXISTS `shop_coupon`;
CREATE TABLE `shop_coupon` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `coupon_title` varchar(255) NOT NULL COMMENT '优惠券名称',
  `coupon_type` enum('1','2') DEFAULT NULL,
  `coupon_pic` varchar(255) NOT NULL,
  `coupon_desc` varchar(255) NOT NULL COMMENT '优惠券描述',
  `start_time` bigint(13) DEFAULT NULL COMMENT '优惠券开始日期',
  `end_time` bigint(13) DEFAULT NULL COMMENT '优惠券截止日期',
  `coupon_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '优惠金额',
  `coupon_limit` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额限制',
  `store_id` int(11) DEFAULT NULL COMMENT '店铺id',
  `coupon_state` int(11) DEFAULT '1' COMMENT '上架与下架状态:1下架，0上架',
  `coupon_storage` int(11) NOT NULL DEFAULT '0' COMMENT '总共数量',
  `coupon_usage` int(11) NOT NULL DEFAULT '0' COMMENT '使用数量',
  `coupon_lock` int(11) NOT NULL DEFAULT '0' COMMENT '是否锁定：1锁定，0未锁定',
  `create_time` bigint(13) DEFAULT NULL COMMENT '添加日期',
  `coupon_class_id` int(11) NOT NULL COMMENT '分类',
  `coupon_click` int(11) NOT NULL DEFAULT '1' COMMENT '点击次数',
  `coupon_print_style` varchar(255) DEFAULT '4STYLE' COMMENT '4STYLE STANDS FOR 4 COUPONS PER A4 PAGE, AND 8STYLE STANDS FOR 8 COUPONS PER A4 PAGE',
  `coupon_recommend` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0不推荐 1推荐到首页',
  `coupon_allowstate` int(1) unsigned DEFAULT '0' COMMENT '审核状态 0为待审核 1已通过 2未通过',
  `coupon_allowremark` varchar(255) DEFAULT NULL COMMENT '审核备注',
  `store_name` varchar(50) DEFAULT NULL COMMENT '店铺名称',
  `coupon_goods_id` int(10) DEFAULT NULL COMMENT '商品分类id',
  PRIMARY KEY (`coupon_id`),
  KEY `store_id` (`store_id`) USING BTREE COMMENT '(null)'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='优惠券';

-- ----------------------------
-- Records of shop_coupon
-- ----------------------------
INSERT INTO `shop_coupon` VALUES ('3', '123', null, '/upload/img/store/slide/1444369802848png', '', '1442564161000', '1446220800000', '12.00', '123.00', '9', '0', '111', '1', '0', '20150914112531', '1', '1', null, '0', '1', null, '京川牦牛', '0');
INSERT INTO `shop_coupon` VALUES ('6', '满5000减500', null, '', '', '1442564161187', '1442564161187', '500.00', '5000.00', '12', '0', '10', '0', '0', '20150914112531', '1', '1', null, '0', '1', null, '新科数码专营店', '0');
INSERT INTO `shop_coupon` VALUES ('7', '满10减1', null, '/upload/img/store/slide/1444036748735jpg', '', '1442564161000', '1448812800000', '1.00', '10.00', '12', '0', '10', '0', '0', '1444036886101', '1', '1', null, '1', '1', null, '新科数码专营店', '0');
INSERT INTO `shop_coupon` VALUES ('9', '满10减2', null, '', '满十块才可以使用', '1442564161187', '1442564161187', '2.00', '10.00', '13', '0', '2000', '1', '0', '20150914112531', '1', '1', null, '0', '1', null, '∑SeHun°の店', '0');
INSERT INTO `shop_coupon` VALUES ('12', '满200减10', null, '', '', '1442564161187', '1442564161187', '10.00', '200.00', '13', '0', '22222', '1', '0', '20150914112531', '1', '1', null, '0', '1', null, '∑SeHun°の店', '0');
INSERT INTO `shop_coupon` VALUES ('14', '满50减5', null, '', '', '1442564161187', '1442564161187', '5.00', '50.00', '13', '0', '33333', '0', '0', '20150914112531', '1', '1', null, '0', '1', null, '∑SeHun°の店', '0');
INSERT INTO `shop_coupon` VALUES ('15', '减减减', null, '/upload/img/store/slide/1448613671998.png', '', '1448640000000', '1448812800000', '10.00', '100.00', '9', '0', '22', '0', '0', null, '1', '1', null, '0', '1', null, '京川牦牛', '0');
INSERT INTO `shop_coupon` VALUES ('16', '满就减', null, '/upload/img/store/slide/1448968624731.png', '', '1448899200000', '1451491200000', '100.00', '999.00', '9', '0', '12', '0', '0', '1448968814230', '1', '1', null, '0', '1', null, '京川牦牛', '0');
