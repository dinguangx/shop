/* liukai 2015-10-22 增加shop_return_log退货处理历史表 */
DROP TABLE IF EXISTS `shop_return_log`;
CREATE TABLE `shop_return_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '退货处理历史索引id',
  `return_id` int(11) NOT NULL COMMENT '退货表id',
  `return_state` varchar(20) NOT NULL COMMENT '退货状态信息',
  `change_state` varchar(20) NOT NULL COMMENT '下一步退货状态信息',
  `state_info` varchar(100) NOT NULL COMMENT '退货状态描述',
  `create_time` bigint(13) DEFAULT NULL COMMENT '处理时间',
  `operator` varchar(30) NOT NULL COMMENT '操作人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=721 DEFAULT CHARSET=utf8 COMMENT='退货处理历史表';


/* liukai 2015-10-30 增加shop_refund_return退款退货表 */
DROP TABLE IF EXISTS `shop_refund_return`;
CREATE TABLE `shop_refund_return` (
  `refund_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `order_id` int(10) unsigned NOT NULL COMMENT '订单ID',
  `order_sn` varchar(50) NOT NULL COMMENT '订单编号',
  `refund_sn` varchar(50) NOT NULL COMMENT '申请编号',
  `store_id` int(10) unsigned NOT NULL COMMENT '店铺ID',
  `store_name` varchar(20) NOT NULL COMMENT '店铺名称',
  `buyer_id` int(10) unsigned NOT NULL COMMENT '买家ID',
  `buyer_name` varchar(50) NOT NULL COMMENT '买家会员名',
  `goods_id` int(10) unsigned NOT NULL COMMENT '商品ID',
  `order_goods_id` int(10) unsigned DEFAULT '0' COMMENT '订单商品ID',
  `goods_name` varchar(50) NOT NULL COMMENT '商品名称',
  `goods_num` int(10) unsigned DEFAULT '1' COMMENT '商品数量',
  `refund_amount` decimal(10,2) DEFAULT '0.00' COMMENT '退款金额',
  `goods_image` varchar(100) DEFAULT NULL COMMENT '商品图片',
  `order_goods_type` tinyint(1) unsigned DEFAULT '1' COMMENT '订单商品类型:1默认2团购商品3限时折扣商品4组合套装',
  `refund_type` tinyint(1) unsigned COMMENT '申请类型:1为退款,2为退货',
  `seller_state` tinyint(1) unsigned COMMENT '卖家处理状态:1为待审核,2为同意,3为不同意',
  `refund_state` tinyint(1) unsigned COMMENT '申请状态:1为处理中,2为待管理员处理,3为已完成',
  `return_type` tinyint(1) unsigned COMMENT '退货类型:1为不用退货,2为需要退货',
  `order_lock` tinyint(1) unsigned COMMENT '订单锁定类型:1为不用锁定,2为需要锁定',
  `goods_state` tinyint(1) unsigned COMMENT '物流状态:1为待发货,2为待收货,3为未收到,4为已收货',
  `create_time` bigint(13) unsigned NOT NULL COMMENT '添加时间',
  `seller_time` bigint(13) unsigned COMMENT '卖家处理时间',
  `admin_time` bigint(13) unsigned COMMENT '管理员处理时间',
  `reason_id` int(10) unsigned DEFAULT '0' COMMENT '原因ID:0为其它',
  `reason_info` varchar(300) DEFAULT '' COMMENT '原因内容',
  `pic_info` varchar(300) DEFAULT '' COMMENT '图片',
  `buyer_message` varchar(300) DEFAULT NULL COMMENT '申请原因',
  `seller_message` varchar(300) DEFAULT NULL COMMENT '卖家备注',
  `admin_message` varchar(300) DEFAULT NULL COMMENT '管理员备注',
  `express_id` int(10) unsigned DEFAULT '0' COMMENT '物流公司编号',
  `express_name` varchar(50) DEFAULT NULL COMMENT '物流公司名称',
  `invoice_no` varchar(50) DEFAULT NULL COMMENT '物流单号',
  `ship_time` bigint(13) unsigned COMMENT '发货时间',
  `delay_time` bigint(13) unsigned COMMENT '收货延迟时间',
  `receive_time` bigint(13) unsigned COMMENT '收货时间',
  `receive_message` varchar(300) DEFAULT NULL COMMENT '收货备注',
  `commis_rate` smallint(6) DEFAULT '0' COMMENT '佣金比例',
  PRIMARY KEY (`refund_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='退款退货表';


/* liukai 2015-10-30 增加shop_refund_reason退款退货原因表 */
DROP TABLE IF EXISTS `shop_refund_reason`;
CREATE TABLE `shop_refund_reason` (
  `reason_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '原因ID',
  `reason_info` varchar(50) NOT NULL COMMENT '原因内容',
  `sort` tinyint(1) unsigned DEFAULT '255' COMMENT '排序',
  `update_time` bigint(10) unsigned NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`reason_id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='退款退货原因表';

INSERT INTO `shop_refund_reason` VALUES ('95', '效果不好不喜欢', '123', '1393480261');
INSERT INTO `shop_refund_reason` VALUES ('96', '商品破损、有污渍', '123', '1393480261');
INSERT INTO `shop_refund_reason` VALUES ('97', '保质期不符', '123', '1393480261');
INSERT INTO `shop_refund_reason` VALUES ('98', '认为是假货', '123', '1393480261');
INSERT INTO `shop_refund_reason` VALUES ('99', '不能按时发货', '123', '1393480261');

/* liukai 2015-11-17 增加shop_order_bill结算表 */
DROP TABLE IF EXISTS `shop_order_bill`;
CREATE TABLE `shop_order_bill` (
  `ob_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '结算',
  `ob_no` varchar(30) NOT NULL COMMENT '结算单编号(年月店铺ID)',
  `ob_start_time` bigint(13) NOT NULL COMMENT '开始日期',
  `ob_end_time` bigint(13) NOT NULL COMMENT '结束日期',
  `ob_order_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '订单金额',
  `ob_shipping_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '运费',
  `ob_order_return_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '退单金额',
  `ob_commis_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '佣金金额',
  `ob_commis_return_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '退还佣金',
  `ob_store_cost_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '店铺促销活动费用',
  `ob_result_totals` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '应结金额',
  `create_time` bigint(13) DEFAULT NULL COMMENT '生成结算单日期',
  `os_month` int(6) DEFAULT NULL COMMENT '结算单年月份',
  `ob_state` int(3) DEFAULT 10 COMMENT '10默认20店家已确认30平台已审核40结算完成',
  `ob_pay_time` bigint(13) DEFAULT NULL COMMENT '付款日期',
  `ob_pay_content` varchar(200) DEFAULT '' COMMENT '支付备注',
  `ob_store_id` int(11) NOT NULL COMMENT '店铺ID',
  `ob_store_name` varchar(50) DEFAULT NULL COMMENT '店铺名',
  PRIMARY KEY (`ob_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='结算表';