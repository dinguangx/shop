/**
 *＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
 *＝                本清理主要用于应用部署时使用              ＝
 *＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
 *清库脚本
 **/

delete from shop_activity_detail           ;
delete from shop_address                   ;
                                           
delete from shop_admin_log                 ;
                                                                                 
delete from shop_attach_picture            ;
delete from shop_attribute                 ;
delete from shop_attribute_value           ;

delete from shop_cart                      ;
delete from shop_complain                  ;
delete from shop_complain_goods            ;
delete from shop_complain_subject          ;
delete from shop_complain_talk             ;
delete from shop_consult                   ;
delete from shop_coupon                    ;
delete from shop_coupon_class              ;
delete from shop_coupon_member             ;
delete from shop_cron                      ;
delete from shop_daddress                  ;
                                          
delete from shop_evaluate_goods            ;
delete from shop_evaluate_store            ;
                               
delete from shop_favorites                 ;
delete from shop_feedback                  ;
                                          
delete from shop_goods                     ;
delete from shop_goods_attr_index          ;

delete from shop_goods_class               ;
delete from shop_goods_class_staple        ;
delete from shop_goods_class_tag           ;
delete from shop_goods_combination         ;
delete from shop_goods_common              ;
delete from shop_goods_images              ;
delete from shop_goods_recommend           ;
delete from shop_goods_spec                ;
delete from shop_goods_spec_index          ;
delete from shop_goods_words               ;
delete from shop_invoice                   ;
delete from shop_mail_msg_temlates         ;
delete from shop_member                    ;
delete from shop_member_grade              ;
                                 
delete from shop_message                   ;

delete from shop_offpay_area               ;
delete from shop_order                     ;
delete from shop_order_address             ;
delete from shop_order_bill                ;
delete from shop_order_common              ;
delete from shop_order_daddress            ;
delete from shop_order_goods               ;
delete from shop_order_invoice             ;
delete from shop_order_log                 ;
delete from shop_order_pay                 ;
delete from shop_order_statis              ;

delete from shop_pd_cash                   ;
delete from shop_pd_log                    ;
delete from shop_pd_recharge               ;
delete from shop_points_cart               ;
delete from shop_points_goods              ;
delete from shop_points_log                ;
delete from shop_points_order              ;
delete from shop_points_orderaddress       ;
delete from shop_points_ordergoods         ;
                                          
delete from shop_rec_position              ;
delete from shop_refund_log                ;
delete from shop_refund_reason             ;
delete from shop_refund_return             ;
delete from shop_rel_goods_recommend       ;
delete from shop_return_log                ;
                                          
delete from shop_salenum                   ;
delete from shop_seller                    ;
delete from shop_seller_group              ;
delete from shop_seller_log                ;
delete from shop_seo                       ;

delete from shop_spec where sp_id!=1       ;
delete from shop_spec_value where sp_id!=1 ;
delete from shop_stat_goods                ;
delete from shop_stat_member               ;
delete from shop_store where store_name !="平台自营" ;
update shop_store set store_id =0 where member_id=0 ;
delete from shop_store_bind_class          ;
delete from shop_store_class               ;
delete from shop_store_cost                ;
delete from shop_store_extend              ;
delete from shop_store_goods_class         ;

delete from shop_store_joinin              ;
delete from shop_store_navigation          ;
delete from shop_store_plate               ;
delete from shop_store_sns_comment         ;
delete from shop_store_sns_setting         ;
delete from shop_store_sns_tracelog        ;
delete from shop_store_watermark           ;
delete from shop_store_words               ;
delete from shop_sys_log                   ;
delete from shop_transport                 ;
delete from shop_transport_extend          ;
delete from shop_type                      ;
delete from shop_type_brand                ;
delete from shop_type_spec                 ;
delete from shop_upload                    ;
delete from shop_web                       ;
delete from shop_web_code                  ;

